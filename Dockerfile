FROM openjdk:21-slim
WORKDIR /app
COPY target/kata-delivery.jar kata-delivery.jar
EXPOSE 8080
CMD ["java", "-jar", "kata-delivery.jar"]