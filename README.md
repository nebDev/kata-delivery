
<<<<<<<<<<-------------------------------------------------------------------------<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<---------------------------------------------

Java: Utilisé comme langage de programmation principal pour le développement de l'application.

Spring Boot: Framework Java pour le développement d'applications Spring facilitant la création d'applications Java autonomes, basées sur des microservices.

Spring MVC: Pour la construction de l'architecture web et le support du modèle MVC (Model-View-Controller) dans Spring.

Architecture basée sur les couches suivantes : 
 - ENTITY pour les tables
 - DTOs pour éviter d'exposer les tables
 - DAO assurer la communication pour la persistance des données
 - CONVETER pour assurer la conversion des classes DTOs en ENTITY 
 - METIER pour définir les méthodes sans les traitements
 - IMPLEMENTE(IMPL) couche dans laquelle sont effectuées les traitements.
 - CONTROLLER dans lesquelles sont exposée les end-points


Spring Data JPA: Pour la gestion des accès aux bases de données relationnelles via Java Persistence API (JPA).

Hibernate: Un framework de persistance pour la gestion des données entre les objets Java et les bases de données relationnelles.

Spring Security: Pour la gestion de l'authentification et de l'autorisation dans l'application.

Thymeleaf: Moteur de template pour la création de vues côté serveur dans les applications web Spring.

Bootstrap: Un framework CSS pour la conception de l'interface utilisateur.

Docker: Pour la conteneurisation de l'application, facilitant ainsi le déploiement et l'exécution dans divers environnements.

H2 Database: Une base de données en mémoire qui pourrait être utilisée pour le développement et les tests.

Git: Système de contrôle de version pour le suivi des modifications du code source.

Maven ou Gradle: Outils de gestion de projet pour la construction, le test et la gestion des dépendances.

********************************************-------------------------------------------------*****************************************--------------------------------------------


#execution de l'application
-> mvn spring-boot:run

#chemin d'accès a l'application 
-> http://localhost:8080/modelivraison/formulaire


#lien de recuperation de l'image docker 
->docker pull nebie/kata-delivery:kata-delivery-push

#

