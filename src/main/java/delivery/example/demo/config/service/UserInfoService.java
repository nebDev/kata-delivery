package delivery.example.demo.config.service;

import delivery.example.demo.config.entity.UserInfo;
import delivery.example.demo.config.repository.UserInfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class UserInfoService implements UserDetailsService {

    @Autowired private UserInfoRepository userInfoRepository;
    public PasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException{
      Optional<UserInfo> userInfo = userInfoRepository.findByEmail(username);
        return userInfo.map(UserInfoDetails::new)
                .orElseThrow(() -> new UsernameNotFoundException("User not found "+ username));
    }

    public String addUser(UserInfo userInfo){
        userInfo.setPassword(passwordEncoder.encode(userInfo.getPassword()));
        userInfo.setUsername(userInfo.getEmail());
        userInfo.setDateSave(new Date());
        userInfo.setStatut(1);
        userInfoRepository.save(userInfo);
        return "User added successfuly...";
    }

    public List<UserInfo> getAlluser(){
        return userInfoRepository.findAll();
    }

    public UserInfo getUser(Integer id){
        return userInfoRepository.findById(id).get();
    }
}
