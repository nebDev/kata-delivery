package delivery.example.demo.config.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoginReturn {
    private String token;
    private String email;
    private Integer status;
}
