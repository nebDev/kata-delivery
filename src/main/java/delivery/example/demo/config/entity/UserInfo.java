package delivery.example.demo.config.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;
    String name;
    @Column(unique = true)
    String email;
    @Column(unique = true)
    String username;
    Integer statut;
    String telephone;
    String password;
    Date dateSave;
    Date dateEdit;
    Date dateDelete;
    private String roles;

   // @Enumerated(EnumType.STRING)
    //Role role;


}
