package delivery.example.demo.config.controller;

import delivery.example.demo.config.entity.AuthRequest;
import delivery.example.demo.config.entity.LoginReturn;
import delivery.example.demo.config.entity.UserInfo;
import delivery.example.demo.config.service.JwtService;
import delivery.example.demo.config.service.UserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/auth")
public class UserController {

    @Autowired
    private UserInfoService userInfoService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtService jwtService;

    // Ajoute un nouvel utilisateur
    @CrossOrigin("*")
    @PostMapping("/adduser")
    public String addUser(@RequestBody UserInfo userInfo){
        return userInfoService.addUser(userInfo);
    }

    // Gère la demande de connexion (login) et génère un token JWT
    @CrossOrigin("*")
    @PostMapping("/login")
    public LoginReturn addUser(@RequestBody AuthRequest authRequest){
        // Authentification de l'utilisateur
        Authentication authenticate = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(authRequest.getEmail(),authRequest.getPassword()));
        if (authenticate.isAuthenticated()){
            // Si l'authentification réussit, génère un token JWT
            LoginReturn loginReturn = new LoginReturn();
            loginReturn.setToken(jwtService.generateToken(authRequest.getEmail()));
            loginReturn.setEmail(authRequest.getEmail());
            loginReturn.setStatus(200);
            return loginReturn;
        }else {
            // En cas d'échec de l'authentification
            throw new UsernameNotFoundException("Invalid user request");
        }
    }

    // Récupère la liste de tous les utilisateurs
    @CrossOrigin("*")
    @GetMapping("/getalluser")
    public List<UserInfo> getAllUser(){
        return userInfoService.getAlluser();
    }

    // Récupère un utilisateur par ID
    @CrossOrigin("*")
    @GetMapping("/getuser/{id}")
    public UserInfo getUser(@PathVariable Integer id){
        return userInfoService.getUser(id);
    }
}