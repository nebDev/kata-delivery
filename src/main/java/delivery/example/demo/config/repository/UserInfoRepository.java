package delivery.example.demo.config.repository;

import delivery.example.demo.config.entity.UserInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserInfoRepository extends JpaRepository<UserInfo,Integer> {

    @Query("select l from UserInfo l where l.email =:email and l.statut = 1")
    Optional<UserInfo> findByEmail(@Param("email") String email);

}
