package delivery.example.demo;

import lombok.Getter;

@Getter
public class StaticVar {

    public static final String DATA_ACTIVE = "Actif";
    public static final String DATA_DELETE = "delete";
    public static final String DATA_INACTIVE = "Inactif";
    public static final String DATA_YES = "YES";
    public static final String DATA_NO = "NO";

}
