package delivery.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import delivery.example.demo.dto.CreneauHoraireDTO;
import delivery.example.demo.metier.CreneauHoraireMetier;

import java.util.List;

@RestController
@RequestMapping("/creneauhoraire")
public class CreneauHoraireController {

    @Autowired private CreneauHoraireMetier creneauHoraireMetier;

    // Ajoute un créneau horaire
    @PostMapping("/save")
    public CreneauHoraireDTO AddLigneFacture(@RequestBody CreneauHoraireDTO creHorDTO) {
        return creneauHoraireMetier.save(creHorDTO);
    }

    // Met à jour un créneau horaire
    @PutMapping("/update")
    public CreneauHoraireDTO UpdateLigneFacture(@RequestBody CreneauHoraireDTO creHorDTO) {
        return creneauHoraireMetier.update(creHorDTO);
    }

    // Recherche un créneau horaire par ID
    @GetMapping("/find/{id}")
    public CreneauHoraireDTO Find(@PathVariable Long id) {
        return creneauHoraireMetier.find(id);
    }

    // Récupère tous les créneaux horaires
    @GetMapping("/findall")
    public List<CreneauHoraireDTO> FindAll() {
        return creneauHoraireMetier.list();
    }

    // Récupère la liste des créneaux horaires activés
    @GetMapping("/listenable")
    public List<CreneauHoraireDTO> listEnable() {
        return creneauHoraireMetier.listEnable();
    }

    // Récupère la liste des créneaux horaires désactivés
    @GetMapping("/listdisable")
    public List<CreneauHoraireDTO> listDisable() {
        return creneauHoraireMetier.listDisable();
    }

    // Supprime un créneau horaire par ID
    @DeleteMapping("/delete/{id}")
    public Boolean Delete(@PathVariable Long id) {
        return creneauHoraireMetier.delete(id);
    }
}
