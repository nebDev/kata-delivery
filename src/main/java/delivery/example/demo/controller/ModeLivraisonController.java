package delivery.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import delivery.example.demo.dto.ModeLivraisonDTO;
import delivery.example.demo.metier.ModeLivraisonMetier;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/modelivraison")
public class ModeLivraisonController {

    @Autowired private ModeLivraisonMetier modeLivraisonMetier;

    // Affiche le formulaire de livraison avec les modes disponibles
    @GetMapping("/formulaire")
    public ModelAndView Formulaire(Model model) {
        List<String> modesLivraison = Arrays.asList("DRIVE", "DELIVERY", "DELIVERY_TODAY", "DELIVERY_ASAP");
        model.addAttribute("modesLivraisonList", modesLivraison);
        return new ModelAndView("formulaire"); // Renvoie la vue formulaire.html
    }

    // Affiche la liste des commandes de livraison
    @GetMapping("/liste")
    public ModelAndView CommandeList(Model model) {
        List<ModeLivraisonDTO> commandeList = modeLivraisonMetier.listEnable();
        model.addAttribute("commandeList", commandeList);
        return new ModelAndView("listeCommande"); // Renvoie la vue listeCommande.html
    }

    // Traite le formulaire de livraison et redirige vers la liste des commandes
    @PostMapping("/save")
    public ModelAndView traiterFormulaireLivraison(@ModelAttribute ModeLivraisonDTO modeLivraison, Model model) {
        List<ModeLivraisonDTO> commandeList = modeLivraisonMetier.listEnable();
        model.addAttribute("commandeList", commandeList);
        modeLivraisonMetier.save(modeLivraison);
        return new ModelAndView("redirect:/modelivraison/liste"); // Redirige vers /modelivraison/liste
    }

    // Met à jour un mode de livraison
    @PutMapping("/update")
    public ModeLivraisonDTO Update(@RequestBody ModeLivraisonDTO modeLivr) {
        return modeLivraisonMetier.update(modeLivr);
    }

    // Recherche un mode de livraison par ID
    @GetMapping("/find/{id}")
    public ModeLivraisonDTO Find(@PathVariable Long id) {
        return modeLivraisonMetier.find(id);
    }

    // Récupère tous les modes de livraison
    @GetMapping("/findall")
    public List<ModeLivraisonDTO> FindAll() {
        return modeLivraisonMetier.findAll();
    }

    // Récupère la liste des modes de livraison activés
    @GetMapping("/listenable")
    public List<ModeLivraisonDTO> listEnable() {
        return modeLivraisonMetier.listEnable();
    }

    // Récupère la liste des modes de livraison désactivés
    @GetMapping("/listdisable")
    public List<ModeLivraisonDTO> listDisable() {
        return modeLivraisonMetier.listDisable();
    }

    // Supprime un mode de livraison par ID
    @DeleteMapping("/delete/{id}")
    public Boolean Delete(@PathVariable Long id) {
        return modeLivraisonMetier.delete(id);
    }
}