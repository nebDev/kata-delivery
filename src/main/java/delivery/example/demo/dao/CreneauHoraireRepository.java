package delivery.example.demo.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import delivery.example.demo.StaticVar;
import delivery.example.demo.entity.CreneauHoraire;

import java.util.List;

@Repository
public interface CreneauHoraireRepository extends JpaRepository<CreneauHoraire, Long> {

    @Query("select l from CreneauHoraire l where l.statut like '" + StaticVar.DATA_ACTIVE + "'")
    List<CreneauHoraire> listEnable();

    @Query("select l from CreneauHoraire l where l.statut like '" + StaticVar.DATA_INACTIVE + "'")
    List<CreneauHoraire> listDisable();    
    
}
