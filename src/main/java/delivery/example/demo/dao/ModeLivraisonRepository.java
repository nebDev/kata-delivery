package delivery.example.demo.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import delivery.example.demo.StaticVar;
import delivery.example.demo.entity.ModeLivraison;

import java.util.List;

@Repository
public interface ModeLivraisonRepository extends JpaRepository<ModeLivraison, Long> {

    @Query("select l from ModeLivraison l where l.statut like '" + StaticVar.DATA_ACTIVE + "'")
    List<ModeLivraison> listEnable();

    @Query("select l from ModeLivraison l where l.statut like '" + StaticVar.DATA_INACTIVE + "'")
    List<ModeLivraison> listDisable();    
    
}
