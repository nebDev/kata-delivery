package delivery.example.demo;

import lombok.Getter;

@Getter
public class ErrorMessage {
    public static final String ERROR_ID_MSG = "id indéfini";
    public static final String ERROR_FK_MSG = " clé étrangère ";
    public static final String NOT_EXIST = " n'existe pas";
    public static final String ERROR_DUPLICATE = " erreur doublon";
    public static final String EXIST = " existe";
    public static final String ERROR_MATRICULE = " matricule";
    public static final String ERROR_EMAIL = " email";
    public static final String LIST_EMPTY = " liste vide";

    public static final String USER_EXISTE = "l'utilisteur existe déja";
    public static final String EMAIL_EXISTE = "Email existe déja";

    public static final String UTILISATEUR_NOT_FOUND_MSG = "Utilisateur " + NOT_EXIST;
    public static final String CRENEAU_HORAIRE_NOT_FOUND_MSG = "Creneau Horaire " + NOT_EXIST;
    public static final String MODE_LIVRAISON_NOT_FOUND_MSG = "Mode Livraison " + NOT_EXIST;

}
