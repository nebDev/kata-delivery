package delivery.example.demo;

import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.IntStream;

public class ApplicationException extends RuntimeException {
    public ApplicationException() { super(); }

    public ApplicationException(String message) {
        super(message);
    }

    public ApplicationException(String message, Exception exception) {
        super(message, exception);
    }

    public ApplicationException (Class clazz, String... searchParamsMap) {
        super(ApplicationException.generateMessage(clazz.getSimpleName(), 
        toMap(String.class, String.class, searchParamsMap)));
    }

    private static String generateMessage(String entity, Map<String, String> searchParams) {
        return StringUtils.capitalize(entity) +
                " n'existe pas pour ce paramètre " +
                searchParams;
    }

    private static <K, V> Map<K, V> toMap(Class<K> keyType, Class<V> valueType, Object... entries) {
        if (entries.length % 2 == 1) throw new IllegalArgumentException("Valeur invalide");
        return IntStream.range(0, entries.length / 2).map(i -> i * 2)
                .collect(HashMap::new,
                        (m, i) -> m.put(keyType.cast(entries[i]), valueType.cast(entries[i + 1])),
                        Map::putAll);
    }
}

