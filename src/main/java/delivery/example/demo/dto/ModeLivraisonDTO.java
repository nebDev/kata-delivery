package delivery.example.demo.dto;

import java.util.Date;

import jakarta.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class ModeLivraisonDTO {

     private static final String FORMAT_DATE = "dd-MM-yyyy HH:mm:ss";

     @JsonProperty("id")
    private Long id;

    @NotNull(message="designation invalide")
    @JsonProperty("designation")
    private String designation;

    @JsonProperty("debut")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = FORMAT_DATE)
    private String debut;

    @JsonProperty("fin")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = FORMAT_DATE)
    private String fin;

    @JsonProperty("dateSave")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = FORMAT_DATE)
    private Date dateSave;

    @JsonProperty("statut")
    private String statut;
    
}
