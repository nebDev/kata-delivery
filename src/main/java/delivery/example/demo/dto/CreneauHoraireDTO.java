package delivery.example.demo.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class CreneauHoraireDTO {

    private static final String FORMAT_DATE = "dd-MM-yyyy HH:mm:ss";

    @JsonProperty("id")
    private Long id;

    @JsonProperty("debut")
    @NotNull(message="debut invalide")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = FORMAT_DATE)
    private Date debut;

    @NotNull(message="fin invalide")
    @JsonProperty("fin")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = FORMAT_DATE)
    private Date fin;

    @JsonProperty("dateSave")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = FORMAT_DATE)
    private Date dateSave;

    @JsonProperty("statut")
    private String statut;
    
}
