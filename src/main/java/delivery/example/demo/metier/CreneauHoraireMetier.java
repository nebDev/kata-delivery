package delivery.example.demo.metier;
import java.util.List;
import delivery.example.demo.dto.CreneauHoraireDTO;

public interface CreneauHoraireMetier {
    CreneauHoraireDTO save(CreneauHoraireDTO c);
    CreneauHoraireDTO find(Long code);
    boolean delete(Long code);
    CreneauHoraireDTO update(CreneauHoraireDTO c);
    List<CreneauHoraireDTO> list();
    List<CreneauHoraireDTO> listEnable();
    List<CreneauHoraireDTO> listDisable();
}
