package delivery.example.demo.metier;

import java.util.List;

import delivery.example.demo.dto.ModeLivraisonDTO;

public interface ModeLivraisonMetier {
    ModeLivraisonDTO save(ModeLivraisonDTO c);
    ModeLivraisonDTO find(Long code);
    boolean delete(Long code);
    ModeLivraisonDTO update(ModeLivraisonDTO c);
    List<ModeLivraisonDTO> findAll();
    List<ModeLivraisonDTO> listEnable();
    List<ModeLivraisonDTO> listDisable();
}
