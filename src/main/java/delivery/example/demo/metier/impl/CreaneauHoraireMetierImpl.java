package delivery.example.demo.metier.impl;

import delivery.example.demo.ApplicationException;
import delivery.example.demo.ErrorMessage;
import delivery.example.demo.StaticVar;
import delivery.example.demo.converter.CreneauHoraireConverter;
import delivery.example.demo.dto.CreneauHoraireDTO;
import delivery.example.demo.entity.CreneauHoraire;
import delivery.example.demo.metier.CreneauHoraireMetier;
import delivery.example.demo.dao.CreneauHoraireRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CreaneauHoraireMetierImpl implements CreneauHoraireMetier {

    @Autowired private CreneauHoraireRepository creneauHoraireRepository;
    @Autowired private CreneauHoraireConverter creneauHoraireConverter;

    @Override
    public CreneauHoraireDTO save(CreneauHoraireDTO creneauHoraireDTO) {
        creneauHoraireDTO.setId(null);
        creneauHoraireDTO.setDateSave(new Date());
        creneauHoraireDTO.setStatut(StaticVar.DATA_ACTIVE);
        CreneauHoraire entite = creneauHoraireConverter.toEntitie(creneauHoraireDTO);
        CreneauHoraire returnedEntity = creneauHoraireRepository.save(entite);
        return creneauHoraireConverter.toDto(returnedEntity);
    }

    @Override
    public CreneauHoraireDTO find(Long code) {
        if(!creneauHoraireRepository.existsById(code))
            throw new ApplicationException(ErrorMessage.CRENEAU_HORAIRE_NOT_FOUND_MSG);
        return creneauHoraireConverter.toDto(creneauHoraireRepository.findById(code).get());
    }

    @Override
    public boolean delete(Long code)
    {
        if(!creneauHoraireRepository.existsById(code))
            throw new ApplicationException(ErrorMessage.CRENEAU_HORAIRE_NOT_FOUND_MSG);
        CreneauHoraire compteComptable = creneauHoraireRepository.getOne(code);
        compteComptable.setStatut(StaticVar.DATA_INACTIVE);
        creneauHoraireRepository.saveAndFlush(compteComptable);
        return true;
    }

    @Override
    public CreneauHoraireDTO update(CreneauHoraireDTO creneauHoraireDTO) {

        if(creneauHoraireDTO.getId()==null || creneauHoraireRepository.findById(creneauHoraireDTO.getId()).get().getStatut().equals(StaticVar.DATA_INACTIVE))
            throw new ApplicationException(ErrorMessage.CRENEAU_HORAIRE_NOT_FOUND_MSG);
        if(creneauHoraireRepository.existsById(creneauHoraireDTO.getId())) {
        creneauHoraireDTO.setDateSave(creneauHoraireRepository.findById(creneauHoraireDTO.getId()).get().getDateSave());
		creneauHoraireDTO.setStatut(StaticVar.DATA_ACTIVE);
        CreneauHoraire creneauHoraire = creneauHoraireConverter.toEntitie(creneauHoraireDTO);
        creneauHoraireRepository.saveAndFlush(creneauHoraire);
            return creneauHoraireConverter.toDto(creneauHoraire);
        }else
            throw new ApplicationException(ErrorMessage.CRENEAU_HORAIRE_NOT_FOUND_MSG);

    }

    @Override
    public List<CreneauHoraireDTO> list() {
        List<CreneauHoraire> entities = creneauHoraireRepository.findAll();
        return entities.stream()
                .map(creneauHoraire -> creneauHoraireConverter
                        .toDto(creneauHoraire)).collect(Collectors.toList());
    }

    @Override
    public List<CreneauHoraireDTO> listDisable() {
        List<CreneauHoraire> entities = creneauHoraireRepository.listDisable();
        return entities.stream()
                .map(creneauHoraire -> creneauHoraireConverter
                        .toDto(creneauHoraire)).collect(Collectors.toList());
    }

    @Override
    public List<CreneauHoraireDTO> listEnable() {
        List<CreneauHoraire> entities = creneauHoraireRepository.listEnable();
        return entities.stream()
                .map(creneauHoraire -> creneauHoraireConverter
                        .toDto(creneauHoraire)).collect(Collectors.toList());
    }
}
