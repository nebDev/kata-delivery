package delivery.example.demo.metier.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import delivery.example.demo.ApplicationException;
import delivery.example.demo.ErrorMessage;
import delivery.example.demo.StaticVar;
import delivery.example.demo.converter.ModeLivraisonConverter;
import delivery.example.demo.dao.ModeLivraisonRepository;
import delivery.example.demo.dto.ModeLivraisonDTO;
import delivery.example.demo.entity.ModeLivraison;
import delivery.example.demo.metier.ModeLivraisonMetier;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ModeLivraisonMetierImpl implements ModeLivraisonMetier {

    @Autowired private ModeLivraisonRepository modeLivraisonRepository;
    @Autowired private ModeLivraisonConverter modeLivraisonConverter;

    @Override
    public ModeLivraisonDTO save(ModeLivraisonDTO modLiv) {
        modLiv.setId(null);
        modLiv.setDateSave(new Date());
        modLiv.setStatut(StaticVar.DATA_ACTIVE);
        ModeLivraison entity = modeLivraisonConverter.toEntitie(modLiv);
        ModeLivraison returnedEntity = modeLivraisonRepository.save(entity);
        return modeLivraisonConverter.toDto(returnedEntity);
    }

    @Override
    public ModeLivraisonDTO find(Long code) {
        if (!modeLivraisonRepository.existsById(code))
            throw new ApplicationException(ErrorMessage.CRENEAU_HORAIRE_NOT_FOUND_MSG);
        return modeLivraisonConverter.toDto(modeLivraisonRepository.findById(code).get());
    }

    @Override
    public boolean delete(Long code) {
        if (!modeLivraisonRepository.existsById(code))
            throw new ApplicationException(ErrorMessage.CRENEAU_HORAIRE_NOT_FOUND_MSG);
        ModeLivraison modLiv = modeLivraisonRepository.findById(code).get();
        modLiv.setStatut(StaticVar.DATA_INACTIVE);
        modeLivraisonRepository.saveAndFlush(modLiv);
        return true;
    }

    @Override
    public ModeLivraisonDTO update(ModeLivraisonDTO modLiv) {
        if (modeLivraisonRepository.existsById(modLiv.getId())){
		    modLiv.setStatut(StaticVar.DATA_ACTIVE);
            ModeLivraison entity = modeLivraisonConverter.toEntitie(modLiv);
            entity = modeLivraisonRepository.saveAndFlush(entity);
            return modeLivraisonConverter.toDto(entity);}
        else {throw new ApplicationException(ErrorMessage.CRENEAU_HORAIRE_NOT_FOUND_MSG);}
    }

    @Override
    public List<ModeLivraisonDTO> findAll() {
        List<ModeLivraison> entities = modeLivraisonRepository.findAll();
        return entities.stream()
                .map(modLiv -> modeLivraisonConverter.toDto(modLiv))
                .collect(Collectors.toList());
    }

    @Override
    public List<ModeLivraisonDTO> listEnable() {
        List<ModeLivraison> entities = modeLivraisonRepository.listEnable();
        return entities.stream()
                .map(modLiv -> modeLivraisonConverter.toDto(modLiv))
                .collect(Collectors.toList());
    }

    @Override
    public List<ModeLivraisonDTO> listDisable() {
        List<ModeLivraison> entities = modeLivraisonRepository.listDisable();
        return entities.stream()
                .map(modLiv -> modeLivraisonConverter.toDto(modLiv))
                .collect(Collectors.toList());
    }

}
