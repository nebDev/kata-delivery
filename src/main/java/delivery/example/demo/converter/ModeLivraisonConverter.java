package delivery.example.demo.converter;

import org.springframework.stereotype.Component;

import delivery.example.demo.dto.ModeLivraisonDTO;
import delivery.example.demo.entity.ModeLivraison;

@Component
public class ModeLivraisonConverter implements DataConverter<ModeLivraison, ModeLivraisonDTO>{

    @Override
    public ModeLivraisonDTO toDto(ModeLivraison modLiv) {
        if (modLiv == null) return null;
        return ModeLivraisonDTO.builder()
                .id(modLiv.getId())
                .designation(modLiv.getDesignation())
                .debut(modLiv.getDebut())
                .fin(modLiv.getFin())
                .statut(modLiv.getStatut())
                .dateSave(modLiv.getDateSave())
                .build();
    }

    @Override
    public ModeLivraison toEntitie(ModeLivraisonDTO modLivDTO) {
        if (modLivDTO == null) return null;
        return ModeLivraison.builder()
                .id(modLivDTO.getId())
                .designation(modLivDTO.getDesignation())
                .debut(modLivDTO.getDebut())
                .fin(modLivDTO.getFin())
                .statut(modLivDTO.getStatut())
                .dateSave(modLivDTO.getDateSave())
                .build();
    }    
}
