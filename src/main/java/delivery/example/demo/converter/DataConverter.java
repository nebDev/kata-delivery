package delivery.example.demo.converter;

public interface DataConverter<ENTITIE, DTO> {
    DTO toDto(ENTITIE entitie);
    ENTITIE toEntitie(DTO dto);
}
