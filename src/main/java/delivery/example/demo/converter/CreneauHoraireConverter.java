package delivery.example.demo.converter;

import org.springframework.stereotype.Component;

import delivery.example.demo.dto.CreneauHoraireDTO;
import delivery.example.demo.entity.CreneauHoraire;

@Component
public class CreneauHoraireConverter implements DataConverter<CreneauHoraire, CreneauHoraireDTO>{

    @Override
    public CreneauHoraireDTO toDto(CreneauHoraire creHaur) {
        if (creHaur == null) return null;
        return CreneauHoraireDTO.builder()
                .id(creHaur.getId())
                .debut(creHaur.getDebut())
                .fin(creHaur.getFin())
                .statut(creHaur.getStatut())
                .dateSave(creHaur.getDateSave())
                .build();
    }

    @Override
    public CreneauHoraire toEntitie(CreneauHoraireDTO creHaurDTO) {
        if (creHaurDTO == null) return null;
        return CreneauHoraire.builder()
                .id(creHaurDTO.getId())
                .debut(creHaurDTO.getDebut())
                .fin(creHaurDTO.getFin())
                .statut(creHaurDTO.getStatut())
                .dateSave(creHaurDTO.getDateSave())
                .build();
    }
    
}
